﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Calculator
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        private bool comma = false, next = false;
        private double first = 0;
        private double second = 0;
        private string ope = null;
        private Button state = null;
        public MainPage()
        {
            InitializeComponent();
        }

        void OnClickNumber(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (next)
            {
                this.result.Text = null;
                state.BackgroundColor = Color.FromHex("#ff9500");
                state.TextColor = Color.White;
                next = false;
            }

            if (this.result.Text == "0")
            {
                if (button.Text == ".")
                {
                    this.result.Text = "0.";
                    comma = true;
                }
                else
                    this.result.Text = button.Text;
            }
            else
            {
                if (button.Text == "." && !comma)
                {
                    this.result.Text += button.Text;
                    comma = true;
                }
                else if (button.Text == "." && comma)
                { }
                else
                    this.result.Text += button.Text;
            }
            second = Double.Parse(this.result.Text);
        }

        void OnClickOperator(object sender, EventArgs e)
        {
            comma = false;
            if (first == 0)
                first = Double.Parse(this.result.Text);
            next = true;
            if (state == null)
            {
                state = (Button)sender;
                state.BackgroundColor = Color.White;
                state.TextColor = Color.FromHex("#ff9500");
            }
            else
            {
                state.BackgroundColor = Color.FromHex("#ff9500");
                state.TextColor = Color.White;
                state = (Button)sender;
                state.BackgroundColor = Color.White;
                state.TextColor = Color.FromHex("#ff9500");
            }
            if (ope == null)
            {
                ope = ((Button)sender).Text;
            }
            else
            {
                switch (ope)
                {
                    case "÷":
                        first = (first / second);
                        break;
                    case "×":
                        first = (first * second);
                        break;
                    case "+":
                        first = (first + second);
                        break;
                    case "-":
                        first = (first - second);
                        break;
                }
                this.result.Text = first.ToString();
                ope = ((Button)sender).Text;
            }
        }

        void OnClickClear(object sender, EventArgs e)
        {
            this.result.Text = "0";
            first = 0;
            second = 0;
            ope = null;
            next = false;
            comma = false;
            if (state != null)
            {
                state.BackgroundColor = Color.FromHex("#ff9500");
                state.TextColor = Color.White;
                state = null;
            }
        }

        void OnClickPercent(object sender, EventArgs e)
        {
            first = Double.Parse(this.result.Text) / 100;
            this.result.Text = first.ToString();
        }

        void OnClickPlusMinus(object sender, EventArgs e)
        {
            first = Double.Parse(this.result.Text) * -1;
            this.result.Text = first.ToString();
        }

        void OnClickRes(object sender, EventArgs e)
        {
            if (next)
            {
                this.result.Text = "0";
                state.BackgroundColor = Color.FromHex("#ff9500");
                state.TextColor = Color.White;
                next = false;
            }
            switch (ope)
            {
                case "÷":
                    first = (first / second);
                    break;
                case "×":
                    first = (first * second);
                    break;
                case "+":
                    first = (first + second);
                    break;
                case "-":
                    first = (first - second);
                    break;
            }
            ope = null;
            comma = false;
            this.result.Text = first.ToString();
        }
    }
}
